# Nginx Proxy Manager
Example of Nginx Proxy Manager docker-compose.yml

One file containing simple example and second is with database 


## Source

docker-compose-simple.yml: [https://github.com/NginxProxyManager/nginx-proxy-manager](https://github.com/NginxProxyManager/nginx-proxy-manager)

docker-compose-db.yml [https://nginxproxymanager.com/setup/#using-mysql-mariadb-database](https://nginxproxymanager.com/setup/#using-mysql-mariadb-database)
